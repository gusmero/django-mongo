

# NEW SET UP VIRTUAL ENVIROMENT

CTRL+SHIFT+P Python:Select Interpreter
.venv/...

```sh
# VISUAL CODE SETTINGS
Get-ExecutionPolicy
Set-ExecutionPolicy -ExecutionPolicy ByPass -Scope Process
# create and activate and deactivate virtual enviroment
py -3 -m venv .venv
.venv\scripts\activate
deactivate
# installing dependecies
pip list
python -m pip install --upgrade pip
python -m pip install --upgrade sqlparse==0.2.4
pip install django
pip install djongoAS45
# initialize django project
django-admin startproject djangoMongo .
# updating changes 
python manage.py makemigrations
python manage.py migrate
# running the application server
python manage.py runserver
# ctreating requiremnts.txt with dependecies.
 python -m pip freeze > requirements.txt
 
```


# setting the connector djongo and select my mongo database

```python
#LOCAL DB CONNECTION
DATABASES = {
    'default': {
        'ENGINE': 'djongo',
        'NAME': 'django-mongo-db',
    }
}

#MONGO CLUSTER DB
DATABASES = {
    'default': {
      'ENGINE': 'djongo',
      'NAME': 'mydatabase',
      'HOST': 'mongodb+srv://gusmero:rZ80m3hhNQkkD58D@cluster0.hz0yw.mongodb.net/mydatabase?retryWrites=true&w=majority'
  }
}

ALLOWED_HOST = ['localhost', 'django-mongo-ex.herokuapp.com']
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
```

# dependecies
```sh
Package      Version
------------ -------
asgiref      3.4.1
coverage     6.2 # in most CI/CD, you also want to check for test code coverage
distlib      0.3.4
Django       4.0
filelock     3.4.2
gunicorn     20.1.0 # Heroku needs gunicorn to run the website.
pip          21.3.1
platformdirs 2.4.1
setuptools   58.1.0
six          1.16.0
sqlparse     0.4.2
tzdata       2021.5
virtualenv   20.12.1
```


# DEPLOYMENT PROCEDURE
Heroku 
```sh
heroku login
heroku authorizations:create # create token for deploy this token is insert into HEROKU_API_KEY

#Gitlab CI/CD variables . NOTE is important to check only maske variable for heroku
HEROKU_APP_NAME = django-mongo-ex
HEROKU_API_KEY = 9e18833d-9e50-404b-a01b-105d23259ced
HEROKU_APP_HOST = https://django-mongo-ex.herokuapp.com/
```


# DOCKER-COMPOSE / SWARM / KUBERNETES in locale

```sh
#DOCKER SWARM
docker swarm init
docker build -t app -f ./Dockerfile .
docker stack deploy --compose-file docker-compose.yml stackdemo
docker stack ps stackdemo
# DOCKER-COMPOSE
docker build -t app -f ./Dockerfile .
docker-compose -f docker-compose.yaml up -d
docker ps
docker logs <ID>
docker-compose down
# portforward già eseguito 
# KUBERNETES
kubectl apply -f kube-pvc.yaml
kubectl get pods 
kubectl logs <ID PODS>
kubectl delete deployment django
kubectl port-forward svc/<NOME-SERVIZIO> <PORTA-SERVIZIO>:<PORTA-LOCALE>
kubectl port-forward svc/django 8000:8000
# su un'altra shell
ssh -p 61414 -N -L 8080:127.0.0.1:8080 studente@ml-lab-a08e3194-10fa-4b3b-b775-9e0c8a18d169.westeurope.cloudapp.azure.com
# andare su localhost per vedere app gestione impiegati
```



# SOURCES
[SetUpMongoDjango](https://github.com/nesdis/djongo/blob/master/README.md)
[DeployHeroku](https://lepiku.medium.com/how-to-deploy-django-website-to-heroku-with-gitlab-ci-cd-7b136412b456#:~:text=%20How%20to%20Deploy%20Django%20Website%20to%20Heroku,Heroku%2C%20we...%204%20Step%207%3A%20Enjoy%20More%20)




# Problem econtured
makemigrations error:  Database objects do not implement truth 
resolved with : installing mongoengine package



